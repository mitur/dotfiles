#!/bin/bash

WD=`pwd`

ln -s $WD/vscode/keybindings.json ~/.config/Code/User/keybindings.json
ln -s $WD/vscode/settings.json ~/.config/Code/User/settings.json
ln -s $WD/profile ~/.profile
ln -s $WD/zprofile ~/.zprofile
ln -s $WD/zshrc ~/.zshrc
ln -s $WD/spacemacs ~/.spacemacs
ln -s $WD/niro.zsh-theme ~/.oh-my-zsh/themes/

ln -s $WD/i3/config ~/.config/i3/config
ln -s $WD/i3/i3blocks.conf ~/.config/i3/i3blocks.conf


