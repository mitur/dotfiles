
export GOPATH="$HOME/go"
export PATH="$HOME/.cargo/bin:$GOPATH/bin:$PATH"

export PATH="$HOME/.cargo/bin:$PATH"

export RUST_SRC_PATH="$(rustc --print sysroot)/lib/rustlib/src/rust/src/"

export VISUAL=vim
export EDITOR="$VISUAL"
